import numpy as np
import random


class ACO:
    def __init__(self, road_size=10, count_ant=6, iteration=50000, print_data=True, matrix=np.zeros([0, 0])):
        self.matrix = matrix
        self.ants = []
        self.road_size = road_size
        self.print_data = print_data
        self.count_ant = count_ant
        self.iteration = iteration
        self.pheromones = np.ones([road_size, road_size])
        for i in range(road_size):
            self.pheromones[i][i] = 0
        if self.matrix.shape[0] == 0:
            self.matrix = np.zeros([road_size, road_size])
        elif not self.matrix.shape[0] == road_size or not self.matrix.shape[0] == self.matrix.shape[1]:
            raise NameError(
                f'you have to send matrix whit size {road_size}*{road_size} but send matrix whit size {self.matrix.shape[0]}*{self.matrix.shape[1]}')
        # if matrix is empty get number to it
        if self.matrix[0][1] == 0:
            for i in range(road_size):
                for j in range(road_size):
                    if i == j or self.matrix[i, j] != 0.0:
                        continue
                    else:
                        self.matrix[i][j] = np.random.randint(2, 50)
                        self.matrix[j][i] = self.matrix[i][j]
        print("************************************ START ************************************")
        print(self.matrix)

    def _calculate(self, node):
        price = 0
        for i in range(1, len(node)):
            price += self.matrix[node[i]][node[i - 1]]
        return price

    # set new price to road
    # TODO : add kam shodan dar tole zaman
    def _set_calculate_road_pheromones(self, road, price):
        for i in range(len(road)):
            self.pheromones[road[i]][road[i - 1]] = price + (self.pheromones[road[i]][road[i] - 1])
            self.pheromones[road[i - 1]][road[i]] = self.pheromones[road[i]][road[i - 1]]

    def get_list_next_place_percent(self, road):
        last_place = road[-1]
        temp = []
        for i in range(len(self.pheromones[last_place])):
            temp.append(self.pheromones[last_place][i])
        for i in road:
            temp[i] = 0
        sum = 0
        for i in temp:
            if not i == 0:
                sum += i
        temp2 = []
        for i in temp:
            if not i == 0:
                temp2.append(i / sum)
            else:
                temp2.append(0)
        return temp2

    @staticmethod
    def _roulette(a):
        ran = random.random()
        b = 0
        for i in range(len(a)):
            b += a[i]
            if ran <= b:
                return i
        return -1

    def _chose_first_one(self):
        temp11 = []
        for i in range(len(self.pheromones[0])):
            for j in range(i, len(self.pheromones[0])):
                if not i == j:
                    temp11.append({'road': (i, j), 'cost': self.pheromones[i][j]})
        sum = 0
        for i in temp11:
            sum += i['cost']
        temp2 = []
        for i in temp11:
            temp2.append(i['cost'] / sum)
        ran = random.random()
        b = 0
        for i in range(len(temp2)):
            b += temp2[i]
            if ran <= b:
                return temp11[i]['road'][0]

    def _reduce_pheromones(self):
        for i in range(self.pheromones.shape[0]):
            for j in range(i, self.pheromones.shape[1]):
                if not j == i:
                    self.pheromones[i][j] = self.pheromones[i][j] * 0.98
                    self.pheromones[j][i] = self.pheromones[i][j]

    def start(self):

        for k in range(1, self.iteration):
            if k % 5 == 0:
                self._reduce_pheromones()

            ant = {'road': [], 'cost': 0}
            for i in range(1, self.road_size):
                if i > 1:
                    nex = self._roulette(self.get_list_next_place_percent(ant['road']))
                    if not nex == -1:
                        ant['road'].append(nex)
                    else:
                        for j in range(len(self.pheromones[i])):
                            if not j in ant['road']:
                                ant['road'].append(j)
                else:
                    a = self._chose_first_one()
                    ant['road'].append(a)

            ant['cost'] = self._calculate(ant['road'])
            # update pheromones
            for i in self.ants:
                # todo diffrent
                self._set_calculate_road_pheromones(i['road'], 1 / i['cost'])
            if self.print_data:
                print(k, ant)
            self.ants.append(ant)
            self.ants = sorted(self.ants, key=lambda ant: ant['cost'])
            self.ants = self.ants[:self.count_ant]
        if self.print_data:
            for i in range(len(self.ants)):
                print(i, self.ants[i])
            print(self.pheromones)

    def get_ants(self):
        self.ants = sorted(self.ants, key=lambda my_list: my_list["cost"])
        return self.ants



# [7, 3, 9, 2, 1, 8, 0, 5, 6]